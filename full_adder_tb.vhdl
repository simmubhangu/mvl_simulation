library work;
USE work.mvl_ternary.all;

ENTITY full_adder_tb IS
END full_adder_tb;

ARCHITECTURE test_behav OF full_adder_tb IS

	component full_adder is
		port (A,B,Cin: in MVL_TRIT;
		     sum,cout: out MVL_TRIT);
	end component;

	signal i0,i1,ci,s,co: MVL_TRIT;
for mapping: full_adder use entity work.full_adder(behav);
BEGIN
mapping: full_adder port map(i0,i1,ci,s,co);
 process
      type pattern_type is record
         --  The inputs of the adder.
         i0, i1, ci : MVL_TRIT;
         --  The expected outputs of the adder.
         s, co : MVL_TRIT;
      end record;
      --  The patterns to apply.
      type pattern_array is array (natural range <>) of pattern_type;
      constant patterns : pattern_array :=
        (('0', '0', '0', '0', '0'),
         ('0', '0', '1', '1', '0'),
         ('0', '0', '2', '2', '0'),
         ('0', '1', '0', '1', '0'),
         ('0', '1', '1', '2', '0'),
         ('0', '1', '2', '0', '1'),
         ('0', '2', '0', '2', '0'),
         ('0', '2', '1', '0', '1'),
         ('0', '2', '2', '1', '1'),
         ('1', '0', '0', '1', '0'),
         ('1', '0', '1', '2', '0'),
         ('1', '0', '2', '0', '1'),
         ('1', '1', '0', '2', '0'),
         ('1', '1', '1', '0', '1'),
         ('1', '1', '2', '1', '1'),
         ('1', '2', '0', '0', '1'),
         ('1', '2', '1', '1', '1'),
         ('1', '2', '2', '2', '1'),
         ('2', '0', '0', '2', '0'),
         ('2', '0', '1', '0', '1'),
         ('2', '0', '2', '1', '1'),
         ('2', '1', '0', '0', '1'),
         ('2', '1', '1', '1', '1'),
         ('2', '1', '2', '2', '1'),
         ('2', '2', '0', '1', '1'),
         ('2', '2', '1', '2', '1'),
         ('2', '2', '2', '0', '2'));
   begin
      --  Check each pattern.
      for i in patterns'range loop
         --  Set the inputs.
         i0 <= patterns(i).i0;
         i1 <= patterns(i).i1;
         ci <= patterns(i).ci;
         --  Wait for the results.
         wait for 10 ns;
         --  Check the outputs.
         assert s = patterns(i).s
            report "bad sum value" severity error;
         assert co = patterns(i).co
            report "bad carray out value" severity error;
      end loop;
      assert false report "end of test" severity note;
      --  Wait forever; this will finish the simulation.
      wait;
   end process;
END test_behav;
