library work;
use work.mvl_ternary.all; --defination of MVL std

--entity dec --

entity full_adder IS
	PORT (A,B,Cin: in MVL_TRIT; SUM,Cout: out MVL_TRIT);
end full_adder;

architecture behav of full_adder is

begin
	process (A ,B,Cin)
	variable x,y,z,p,q,r: MVL_TRIT;
	begin
	--behavior of ternary full adder circuit
	-- used to generate the sum value -- 
	x:=tgate(A,cycle(A,'1'),cycle(A,'2'),B);
	y:=cycle(x,'1');
	z:=cycle(x,'2');

	--used to generate the caryy value
	
	p:=tgate('0',tgate('0','0','1',B),tgate('0','1','1',B),A);
	q:=tgate(tgate('0','0','1',B),tgate('0','1','1',B),'1',A);
	r:=tgate(tgate('0','1','1',B),'1',tgate('0','1','2',B),A);
	
	sum<=tgate(x,y,z,Cin); --sum
	cout<=tgate(p,q,r,Cin); -- carry out

	end process;
end behav;
