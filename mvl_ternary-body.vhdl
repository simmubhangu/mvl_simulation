package body mvl_ternary is

  -------------------------------------------------------------------
  -- local types
  -------------------------------------------------------------------
  type mvlutrit_table is array(MVL_UTRIT, MVL_UTRIT) of MVL_UTRIT;

  -------------------------------------------------------------------
  -- resolution function
  -------------------------------------------------------------------
  constant resolution_table : mvlutrit_table := (
    --      ---------------------------------------------------------
    --      |  U    X    0    1    Z    W    2    H    -        |   |
    --      ---------------------------------------------------------
             ('U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U'),  -- | U |
             ('U', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'),  -- | X |
             ('U', 'X', '0', 'X', '0', '0', 'X', 'X', 'X'),  -- | 0 |
             ('U', 'X', 'X', '1', '1', '1', 'X', 'X', 'X'),  -- | 1 |
             ('U', 'X', '0', '1', 'Z', 'W', '2', 'H', 'X'),  -- | Z |
             ('U', 'X', '0', '1', 'W', 'W', '2', 'W', 'X'),  -- | W |
             ('U', 'X', 'X', 'X', '2', '2', '2', 'W', 'X'),  -- | 2 |
             ('U', 'X', 'X', 'X', 'H', 'W', 'W', 'H', 'X'),  -- | H |
             ('U', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X')   -- | - |
             );

 function resolved (s : MVL_UTRIT_VECTOR) return MVL_UTRIT is
    variable result : MVL_UTRIT := 'Z';  -- weakest state default
  begin
    -- the test for a single driver is essential otherwise the
    -- loop would return 'X' for a single driver of '-' and that
    -- would conflict with the value of a single driver unresolved
    -- signal.
    if (s'length = 1) then return s(s'low);
    else
      for i in s'range loop
        result := resolution_table(result, s(i));
      end loop;
    end if;
    return result;
  end resolved;

    -------------------------------------------------------------------
  -- tables for logical operations
  -------------------------------------------------------------------

  -- truth table for "and" function
  constant min_table : mvlutrit_table := (
    --      ----------------------------------------------------
    --      |  U    X    0    1    Z    W    2    H    -         |   |
    --      ----------------------------------------------------
             ('U', 'U', '0', 'U', 'U', 'U', 'U', 'U', 'U'),  -- | U |
             ('U', 'X', '0', 'X', 'X', 'X', 'X', 'X', 'X'),  -- | X |
             ('0', '0', '0', '0', '0', '0', '0', '0', '0'),  -- | 0 |
             ('U', 'X', '0', '1', 'X', 'X', '1', '1', 'X'),  -- | 1 |
             ('U', 'X', '0', 'X', 'X', 'X', 'X', 'X', 'X'),  -- | Z |
             ('U', 'X', '0', 'X', 'X', 'X', 'X', 'X', 'X'),  -- | W |
             ('X', 'X', '0', '1', 'X', 'X', '2', 'X', 'X'),  -- | 2 |
             ('U', 'X', '0', '1', 'X', 'X', 'X', '1', 'X'),  -- | H |
             ('U', 'X', '0', 'X', 'X', 'X', 'X', 'X', 'X')   -- | - |
             );

    -- truth table for "max" function	     
  constant max_table : mvlutrit_table := (
    --      ----------------------------------------------------
    --      |  U    X    0    1    Z    W    2    H    -         |   |
    --      ----------------------------------------------------
             ('U', 'U', 'U', '1', 'U', 'U', 'U', '1', 'U'),  -- | U |
             ('U', 'X', 'X', '1', 'X', 'X', 'X', '1', 'X'),  -- | X |
             ('U', 'X', '0', '1', 'X', 'X', '2', '1', 'X'),  -- | 0 |
             ('1', '1', '1', '1', '1', '1', '2', '1', '1'),  -- | 1 |
             ('U', 'X', 'X', '1', 'X', 'X', 'X', '1', 'X'),  -- | Z |
             ('U', 'X', 'X', '1', 'X', 'X', 'X', '1', 'X'),  -- | W |
             ('U', 'X', '2', '2', 'X', 'X', '2', '1', 'X'),  -- | 2 |
             ('1', '1', '1', '1', '1', '1', '1', '1', '1'),  -- | H |
             ('U', 'X', 'X', '1', 'X', 'X', 'X', '1', 'X')   -- | - |
             );

	      -- truth table for "xor" function
  constant cycle_table : mvlutrit_table := (
    --      ----------------------------------------------------
    --      |  U    X    0    1    Z    W    2    H    -         |   |
    --      ----------------------------------------------------
             ('U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U'),  -- | U |
             ('U', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'),  -- | X |
             ('U', 'X', '0', '1', 'X', 'X', '2', '1', 'X'),  -- | 0 |
             ('U', 'X', '1', '2', 'X', 'X', '0', '0', 'X'),  -- | 1 |
             ('U', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'),  -- | Z |
             ('U', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'),  -- | W |
             ('U', 'X', '2', '0', 'X', 'X', '1', 'X', 'X'),  -- | 2 |
             ('U', 'X', '1', '0', 'X', 'X', 'X', '0', 'X'),  -- | H |
             ('U', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X')   -- | - |
             );

   -- function operators

  function min (l :MVL_TRIT; r : MVL_TRIT) return MVL_TRIT is 
  begin
	  return (min_table(l,r));
  end min;

  function max (l :MVL_TRIT; r : MVL_TRIT) return MVL_TRIT is
  begin
	  return (max_table(l,r));
  end max;

  function cycle (l :MVL_TRIT; r : MVL_TRIT) return MVL_TRIT is
  begin
	  return (cycle_table(l,r));
  end cycle;


  function tgate (l0,l1,l2:MVL_TRIT; r : MVL_TRIT) return MVL_TRIT is
  begin
	if(r='U' or r='X' or r='Z') then return 'X';
	elsif r='0' then return l0;
	elsif r='1' then return l1 ;
	elsif r='2' then return l2;
	end if;
   end tgate;
	     
end mvl_ternary;
