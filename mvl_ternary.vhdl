package mvl_ternary is
	type MVL_UTRIT is ( 'U',       -- Uninitialized
                       'X',             -- Forcing  Unknown
                       '0',             -- Forcing  0
                       '1',             -- Forcing  1
		       'Z',             -- High impedence
                       'W',             -- Weak     Unknown
		       '2',             -- forcing  2
                       'H',             -- Weak     1
                       '-'              -- Don't care
                       );
	type MVL_UTRIT_VECTOR is array (NATURAL range <>) of MVL_UTRIT;

  -------------------------------------------------------------------
  -- resolution function
  -------------------------------------------------------------------
  function resolved (s : MVL_UTRIT_VECTOR)return MVL_UTRIT;

  -------------------------------------------------------------------
  -- logic state system  (resolved)
  -------------------------------------------------------------------
  subtype MVL_TRIT is resolved MVL_UTRIT;

  type MVL_TRIT_VECTOR is array (NATURAL range <>) of MVL_TRIT;


  function min  (l :MVL_TRIT; r : MVL_TRIT) return MVL_TRIT;
  function max  (l :MVL_TRIT; r : MVL_TRIT) return MVL_TRIT;
  function tgate (l0,l1,l2 :MVL_TRIT; r : MVL_TRIT) return MVL_TRIT;
  function cycle (l :MVL_TRIT; r : MVL_TRIT) return MVL_TRIT;
  --function "xor"(l :MVL_TRIT; r : MVL_TRIT) return MVL_TRIT;

end mvl_ternary;
